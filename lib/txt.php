<?php
	
	function txt_style_code($txt, $style)
	{
		return '<span style="' . $style . '">' . $txt . '</span>';
	}
	
	function txt_class_code($txt, $class)
	{
		return '<span class="' . $class . '">' . $txt . '</span>';
	}
	
	function small_caps_code($txt)
	{
		return txt_class_code($txt, 'small_caps');
	}
	
	function small_caps($txt)
	{
		echo small_caps_code($txt);
	}
	
	function error_code($txt)
	{
		return txt_class_code($txt, 'error');
	}
	
	function error($txt)
	{
		echo error_code($txt);
	}
	
	function warning_code($txt)
	{
		return txt_class_code($txt, 'warning');
	}
	
	function warning($txt)
	{
		echo warning_code($txt);
	}
	
	function todo_code($txt)
	{
		return txt_class_code($txt, 'todo');
	}
	
	function todo($txt)
	{
		echo todo_code($txt);
	}
	
	function wip_code($txt)
	{
		return txt_class_code($txt, 'wip');
	}
	
	function wip($txt)
	{
		echo wip_code($txt);
	}
?>
