<?php
	
	function img_code($img, $height = "auto", $width = "auto")
	{
		if ($height === "auto" and $width === "auto")
		{
			return '<img src="' . $img . '" alt="' . $img . '" class="middle">';
		}
		else if ($height === "auto")
		{
			return '<img src="' . $img . '" alt="' . $img . '" width="' . $width . '" class="middle">';
		}
		else if ($width === "auto")
		{
			return '<img src="' . $img . '" alt="' . $img . '" height="' . $height . '" class="middle">';
		}
		else
		{
			return '<img src="' . $img . '" alt="' . $img . '" height="' . $height . '" width="' . $width . '" class="middle">';
		}
	}
	
	function img($img, $height = "auto", $width = "auto")
	{
		echo img_code($img, $height, $width);
	}
	
	function url_code($url, $url_txt = '', $url_title = '')
	{
		if ($url_txt === '') { $url_txt = $url; }
		if ($url_title === '') { $url_title = $url_txt; }
		return '<a href="'. $url .'" title="' . $url_title . '">' . $url_txt . '</a>';
	}
	
	function url($url, $url_txt = '', $url_title = '')
	{
		echo url_code($url, $url_txt, $url_title);
	}
	
	function img_txt_code($img, $txt, $height = "auto", $width = "auto")
	{
		return img_code($img, $height, $width) . '&nbsp;' . $txt;
	}
	
	function img_txt($img, $txt, $height = "auto", $width = "auto")
	{
		echo img_txt_code($img, $txt, $height, $width);
	}
	
	function img_url_code($img, $url, $url_txt, $url_title = '', $height = "auto", $width = "auto")
	{
		return img_txt_code($img, url_code($url, $url_txt, $url_title), $height, $width);
	}
	
	function img_url($img, $url, $url_txt, $url_title = '', $height = "auto", $width = "auto")
	{
		echo img_url_code($img, $url, $url_txt, $url_title, $height, $width);
	}
	
	function img_txt_description_code($img, $txt, $description, $height = "auto", $width = "auto")
	{
		$r = '';
		$r .= '<table>';
			$r .= '<tr>';
				$r .= '<td rowspan="2"> ' . img_code($img, $height, $width) . ' </td>';
				$r .= '<td> </td>';
				$r .= '<td> '. $txt .' </td>';
			$r .= '</tr>';
			$r .= '<tr>';
				$r .= '<td> </td>';
				$r .= '<td> <em>'. $description .'</em> </td>';
			$r .= '</tr>';
		$r .= '</table>';
		return $r;
	}
	
	function img_txt_description($img, $txt, $description, $height = "auto", $width = "auto")
	{
		echo img_txt_description_code($img, $txt, $description, $height, $width);
	}
	
	function img_url_description_code($img, $url, $url_txt, $description, $url_title = '', $height = "auto", $width = "auto")
	{
		if ($url_title === '') { $url_title = $url_txt; }
		return img_txt_description_code($img, '<a href="'. $url .'" title="' . $url_title . '">' . $url_txt . '</a>', $description, $height, $width);
	}
	
	function img_url_description($img, $url, $url_txt, $description, $url_title = '', $height = "auto", $width = "auto")
	{
		echo img_url_description_code($img, $url, $url_txt, $description, $url_title, $height, $width);
	}
	
?>
