<?php
	
	function form_begin()
	{
		echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '">';
	}
	
	function form_end()
	{
		echo '</form>';
	}
	
	function form_input_text($name, $size = 30)
	{
		echo '<input type="text" size="' . $size . '" name="' . $name . '">';
	}
	
	function form_submit($value)
	{
		echo '<input type="submit" value="' . $value . '">';
	}
	
?>
