<!DOCTYPE html>
<html lang="fr">
	
	<?php require_once('include/include.php'); ?>
	<?php require_once('lib/lib.php'); ?>
	<?php require_once(data_or_demo() . '/include.php'); ?>
	
	<head>
		<meta charset="utf-8">
		<title><?php website_title(); ?></title>
		<meta name="keywords" content="hnc, saoirse, C++, C++11, OpenScop, Source-to-Source Compilation, Polyhedral Compiler">
		<meta name="description" content="Lénaïc Bagnères's Web site">
		<meta name="author" content="Lénaïc Bagnères">
		<link rel="shortcut icon" type="image/png" href="<?php echo data_or_demo(); ?>/favicon.png">
		<link rel="stylesheet" href="include/css.css" type="text/css" media="screen">
	</head>
	
	<body>
		
		<header>
			<?php include(data_or_demo() . '/header.php'); ?>
		</header>
		
		<header>
			<?php include('include/header_menu.php'); ?>
		</header>
		
		<div>
			<aside>
				<nav>
					<?php include('include/menu.php'); ?>
				</nav>
			</aside>
			
			<?php
				
				// Page "logos"
				if (isset($_GET['section']) == false && isset($_GET['page']) && $_GET['page'] == 'logos')
				{
					include('img/logos/include.php');
					echo '<h1>' . img_txt_code('img/48x48/' . $page_img, $page_title) . '</h1>';
					include('img/logos/article.php');
				}
				// User's section & page
				else
				{
					$section = isset($_GET['section']) ? $_GET['section'] : 'main';
					$section_path = get_section_path($section);
					$pages = get_pages($section_path);
					$page_path = isset($_GET['page']) ? get_page_path($_GET['page']) : '';
					
					// Section title
					if ($section_path != '')
					{
						include($section_path . '/include.php');
						echo '<h1>' . img_txt_code('img/48x48/' . $section_img, $section_title) . '</h1>';
					}
					
					// Section error
					if ($section_path === '')
					{
						echo '<article>';
						error('Section "' . htmlentities($_GET["section"]) . '" does no exist.');
						echo '</article>';
						include(data_or_demo() . '/00_main/article.php');
					}
					
					// Section article
					if ($section_path != '')
					{
						include($section_path . '/article.php');
					}
					
					// Pages
					if (isset($_GET['page']) === false)
					{
						foreach ($pages as $page)
						{
							include ($section_path . '/' . $page . '/include.php');
							img_url_description('img/48x48/' . $page_img, local_link($section, substr($page, 3)), $page_title, $page_description);
						}
					}
					
					// Page article
					if ($page_path != '')
					{
						include($page_path . '/include.php');
						echo '<h1>' . img_txt_code('img/48x48/' . $page_img, $page_title) . '</h1>';
						include($page_path . '/article.php');
					}
					
					// Page error
					if (isset($_GET['page']) && $page_path === '')
					{
						echo '<article>';
						error('Page "' . htmlentities($_GET['page']) . '" does no exist.</error>');
						echo '</article>';
					}
					
					// Pages
					if (isset($_GET['page']))
					{
						echo '<br>';
						img_txt('img/32x32/newspaper.png', '<b>Others pages:</b>'); echo '<br>';
						foreach ($pages as $page)
						{
							echo '<table><tr><td>';
							include ($section_path . '/' . $page . '/include.php');
							img_txt('img/32x16/' . $page_img, url_code(local_link($section, substr($page, 3)), $page_title) . ' <i>(' . $page_description . ')<i>');
							echo '</td></tr></table>';
						}
						echo '<br>';
					}
				}
				
			?>
		</div>
		
		<footer>
			<?php include(data_or_demo() . '/footer.php'); ?>
		</footer>
		
	</body>
	
</html>
