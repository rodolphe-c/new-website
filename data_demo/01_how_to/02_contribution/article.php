<article>
	
	<p>
		If you are interested by modifying / contributing to <i>new-website</i> =) just ask me using the <?php url(local_link('contact'), 'contact section'); ?> of this website (or by sending me an email directly). I can develop for you (or help you) to fix bugs or bring new features.
	</p>
	
	<p>
		<i>new-website</i> uses <?php url('https://gitorious.org/', 'Gitorious'); ?> to host its source code. You can clone <?php url('https://gitorious.org/new-website/new-website', 'new-website project'); ?> on Gitorious&nbsp;; do your modification, test them (in the <?php url(local_link('tests'), 'tests section'); ?> if appropriate), push them and ask a merge request.
	</p>
	
	<p>
		Thank you for any contribution! =)
	</p>
	
</article>
