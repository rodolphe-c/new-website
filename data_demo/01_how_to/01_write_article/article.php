<article>
	
	<p>
		The user can modify content of <i>data</i> directory. If the modification of files in other directories is needed, you should consider contribute to <i>new-website</i>.
	</p>
	
	<h2>Add a section</h2>
	
	<p>
		To add a section, create a directory in <i>data</i> directory with a specific format: <code>xx_name_of_section</code> with <code>xx</code> an integer to sort section for display. This integer does not appear in the website, nor the url.
	</p>
	
	<p>
		Within the section directory add a PHP file named <code>include.php</code> with the following code:
		<?php
			line_left
			(
				'&lt;?php' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$section_title = \'Section name\';' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$section_img = \'section_icon.png\';' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$section_main = true; // To display section in header menu' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$section_secondary = false; // To display section in menu' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'?>\''
			);
		?>
	</p>
	
	<p>
		Within the section directory add a PHP file named <code>article.php</code> with the following code:
		<?php
			line_left
			(
				'&lt;article>' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . 'Type the article of the section using HTML and PHP.' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&lt;/article>\''
			);
		?>
	</p>
	
	<h2>Add an article</h2>
	
	<p>
		The creation of an article and a section is very similar. <br>
		To add an article, create a directory in the section directory using the previous format: <code>xx_name_of_section</code> with <code>xx</code>.
	</p>
	
	<p>
		Within the article directory add a PHP file named <code>include.php</code> with the following code:
		<?php
			line_left
			(
				'&lt;?php' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$page_title = \'Article title\';' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$page_img = \'article_icon.png\';' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$page_description = \'Article description\';' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '$page_link_in_menu = true; // To display article in menu' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'?>\''
			);
		?>
	</p>
	
	<p>
		Within the article directory add a PHP file named <code>article.php</code> with the following code:
		<?php
			line_left
			(
				'&lt;article>' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . 'Type the article using HTML and PHP.' . '<br>' .
				'&nbsp;&nbsp;&nbsp;&nbsp;' . '<br>' .
				'&lt;/article>\''
			);
		?>
	</p>
	
</article>
