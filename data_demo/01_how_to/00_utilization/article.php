<article>
	
	<p>
		Only few prerequisites are needed to use <i>new-website</i>: <br>
		- Clone a git repository <br>
		- Use a PHP server like Apache <br>
		- Write HTML and call PHP functions <br>
	</p>
	
	
	<h2>Clone git repository of <i>new-website</i></h2>
	
	<p>
		From the <?php img_url('img/16x16/gitorious.png', 'https://gitorious.org/new-website/new-website', 'git repository of <i>new-website</i>') ?>, you can clone the repository:
		<?php
			line_left('<code>git clone https://gitorious.org/new-website/new-website.git</code>');
		?>
		Copy <i>new-website</i> directory into the directory of your HTTP server (<code>/var/www</code> or <code>/var/www/html</code> for Apache2 on GNU/Linux). <br>
		Run your browser to see your project: <?php url('http://127.0.0.1', 'http://127.0.0.1') ?>
	</p>
	
	
	<h2>General architecture of <i>new-website</i></h2>
	
	<p>
		The user can modify content of <i>data</i> directory. If the modification of files in other directories is needed, you should consider contribute to <i>new-website</i>.
	</p>
	
	<p>
		The <i>data_demo</i> directory contains data of <i>new-website</i>. These data are ignored if is not empty <i>data</i> directory.
	</p>
	
	<p>
		<i>files</i> and <i>files_demo</i> directories work like <i>data</i> and <i>data_demo</i> directories. To share files, copy them into <code>files/section/article/file.ext</code> or <code>files/section/file.ext</code> and get a link with a PHP function: <code>local_link_file('section', 'article', 'file.ext')</code> or <code>local_link_file('section', 'file.ext')</code>.
	</p>
	
	<p>
		The <i>img</i> directory contains all icons and logos.
	</p>
	
	<p>
		The <i>include</i> directory and <i>index.php</i> contain the struture of the website: header, menu, footer, ...
	</p>
	
	<p>
		The <i>lib</i> directory contains useful PHP functions.
	</p>
	
</article>
