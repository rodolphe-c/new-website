<small>

Copyright © 2013,2014 Lénaïc <?php small_caps('Bagnères'); ?>, hnc@singularity.fr & David <?php small_caps('Guinehut'); ?>, dvdg@singularity.fr | Texts in <?php img_url('img/16x16/creative_commons.png', 'http://creativecommons.org/licenses/by-sa/3.0/', 'CC BY-SA 3.0', 'Creative Common Attribution-ShareAlike 3.0 License'); ?>
<br>

Icons used are the <?php img_url('img/16x16/oxygen.png', 'http://techbase.kde.org/Projects/Oxygen', 'Oxygen icons'); ?> (<?php img_url('img/16x16/creative_commons.png', 'http://creativecommons.org/licenses/by-sa/3.0/', 'CC BY-SA 3.0', 'Creative Common Attribution-ShareAlike 3.0 License'); ?> and <?php img_url('img/16x16/gnu.png', 'http://www.gnu.org/licenses/lgpl.html', 'GNU LGPL', 'GNU Lesser General Public License'); ?>)
<br>

For logos, see the <?php img_url('img/16x16/gnu.png', './index.php?page=logos', 'logos licenses'); ?>
<br>

Powered by <?php img_url('img/16x16/konqueror.png', 'https://gitorious.org/new-website', 'New Website'); ?>

</small>
