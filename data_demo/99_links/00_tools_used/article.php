<article>
	
	<h2><?php img('img/32x32/toile_libre.png'); ?> <?php url('http://www.toile-libre.org/', 'Toile-Libre.org'); ?></h2>
	
	<p>
		<i>new-website</i> is hosted by Toile-Libre.org.
	</p>
	
	<p>
		<?php
			multi_column_2
			(
				'From ' . url_code('http://www.toile-libre.org/en/node/71', 'Toile-Libre.org') . ':<br><br>
				We are an association that offers a web hosting service, free, accepting donations and using only free software. <br>
				<br>
				We\'re trying to provide to all more or less rare services without profitability worries.'
			,
				'Depuis ' . url_code('http://www.toile-libre.org/fr/introduction', 'Toile-Libre.org') . ' :<br><br>
				Toile-Libre est une association qui propose des services d\'hébergement internet à prix libre, en utilisant les technologies et logiciels libres. Nous fournissons pour tou-te-s et avec tou-te-s, tout type de service internet plus ou moins rares loin de la sphère marchande. <br>
				<br>
				Les hébergements que nous proposons actuellement sont présentés dans la section services, ils seront amenés à être completés à l\'avenir selon vos besoins et vos envies !<br>
				<br>
				Nous fonctionnons sur le principe du prix libre et sur les cotisations des adhérents pour rester autonomes et nous développer (proposer à tou-te-s ceux-celles qui le veulent les services nécéssaire pour une Toile Libre). C\'est ce modèle financier qui nous permet d\'allier notre éthique : indépendance, réappropriation et accès universel, aux réalitées de ce monde marchand où un kilo de tomates a la même valeur à quelques facteurs près qu\'un site hebergé sur internet'
			);
		?>
	</p>
	
	
	<h2><?php img('img/32x32/git.png'); ?> <?php url('http://git-scm.com/', 'Git'); ?></h2>
	
	<p>
		<i>new-website</i> uses Git as version control system.
	</p>
	
	<p>
		From <?php url('http://git-scm.com/', 'Git'); ?>: <br>
		Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.
	</p>
	
	
	<h2><?php img('img/32x32/gitorious.png'); ?> <?php url('https://gitorious.org/', 'Gitorious'); ?></h2>
	
	<p>
		<i>new-website</i> source code is hosted by Gitorious.
	</p>
	
	<p>
		From <?php url('https://gitorious.org/', 'Gitorious'); ?>: <br>
		Git hosting and collaboration software that you can install yourself.
	</p>
	
	
	<h2><?php img('img/32x32/html.png'); ?> <?php url('http://en.wikipedia.org/wiki/HTML', 'HTML'); ?></h2>
	
	<p>
		<i>new-website</i> uses HTML 5 and CSS 3.
	</p>
	
	<p>
		<?php
			multi_column_2
			(
				'From ' . url_code('http://en.wikipedia.org/wiki/HTML', 'Wikipedia') . ':<br>
				The standard markup language used to create web pages.'
			,
				'Depuis ' . url_code('http://fr.wikipedia.org/wiki/Hypertext_Markup_Language', 'Wikipédia') . ' :<br>
				Format de données conçu pour représenter les pages web.'
			);
		?>
	</p>
	
	
	<h2><?php img('img/32x32/php.png'); ?> <?php url('http://php.net/', 'PHP'); ?></h2>
	
	<p>
		<i>new-website</i> uses PHP 5.
	</p>
	
	<p>
		From <?php url('http://php.net/', 'PHP'); ?>: <br>
		A popular general-purpose scripting language that is especially suited to web development.
	</p>
	
	
	<h2><?php img('img/32x32/oxygen.png'); ?> <?php url('https://techbase.kde.org/Projects/Oxygen', 'Oxygen icons'); ?></h2>
	
	<p>
		<i>new-website</i> uses Oxygen icons.
	</p>
	
	<p>
		A set of computer icons used in <?php url('https://www.kde.org/', 'KDE'); ?> project.
	</p>
	
</article>
